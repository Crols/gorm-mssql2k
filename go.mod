module gitlab.com/Crols/gorm-mssql2k

go 1.16

require (
	github.com/minus5/gofreetds v0.0.0-20200826115934-6705a38c49ca
	github.com/stretchr/testify v1.7.0 // indirect
	gorm.io/gorm v1.21.3
)
