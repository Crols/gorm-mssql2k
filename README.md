# GORM Microsoft SQL 2000 Driver

Supported:
- Create
- Update
- Delete
- Transactions

Not supported
- Save

## Usage

```go
import (
  "gitlab.com/Crols/gorm-mssql2k"
  "gorm.io/gorm"
)

// github.com/denisenkom/go-mssqldb
dsn := "Server=localhost:1433;Database=gorm;User Id=gorm;Password=LoremIpsum42;"
db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
```

Checkout [https://gorm.io](https://gorm.io) for details.
